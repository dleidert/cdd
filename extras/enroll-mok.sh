#!/bin/sh
# vim: set ts=4 sw=4 ai si et:

set -x

modprobe efivarfs || true

mount -t efivarfs efivarfs /target/sys/firmware/efi/efivars || true

test -e /target/root/mok.der || exit 0
in-target echo "Enroll DKMS mok.der key" >&2
in-target sh -c "printf 'root\nroot\n' | /usr/bin/mokutil --import /root/mok.der"

umount /target/sys/firmware/efi/efivars || true

exit 0
