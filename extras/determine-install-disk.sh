#!/bin/sh
# vim: set ts=4 sw=4 ai si et:

set -x

ls -lA /dev/disk/by-path/

if ls /dev/disk/by-path/virtio-* >/dev/null 2>&1
then
    devs="$(ls /dev/disk/by-path/virtio-* | grep -v 'part' | head -n1)"
elif ls /dev/disk/by-path/*-nvme-* >/dev/null 2>&1
then
    devs="$(ls /dev/disk/by-path/*-nvme-* | grep -v 'part' | head -n1)"
elif ls /dev/disk/by-path/*-ata-* >/dev/null 2>&1
then
    devs="$(ls /dev/disk/by-path/*-ata-*  | grep -v 'part' | head -n1)"
fi

if [ -n "${devs}" ]
then
    dev="$(readlink -f ${devs})"
    debconf-set partman-auto/disk "${dev}"
    debconf-set grub-installer/bootdev "${dev}"
fi

exit 0
