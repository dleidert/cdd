#!/bin/bash

set -e

qemu-system-x86_64 \
	-enable-kvm \
	-cpu host \
	-smp 2 \
	-m 4G \
	-device virtio-net-pci,netdev=net0 \
	-netdev user,id=net0,hostfwd=tcp::10022-:22,hostfwd=tcp::10943-:943,hostfwd=tcp::10443-:443,hostfwd=udp::11194-:1194 \
	-usb -device usb-tablet \
	-hda images/qemu-test.hd.img

exit 0
