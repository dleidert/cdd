#!/bin/bash

set -e

RUNONLY=0
NETDEV="virtio-net-pci"

while getopts besS:V:hM:irn opt
do
  case $opt in
    b)
      # enable boot menu
      OPTIONS="${OPTIONS} -boot menu=on"
      ;;
    e)
      # enable EFI
      OPTIONS="${OPTIONS} -bios /usr/share/ovmf/OVMF.fd"
      ;;
    s)
      # enable Secure Boot
      OPTIONS="${OPTIONS} -machine q35,smm=on"
      OPTIONS="${OPTIONS} -global driver=cfi.pflash01,property=secure,value=on"
      OPTIONS="${OPTIONS} -drive if=pflash,format=raw,readonly=on,file=/usr/share/OVMF/OVMF_CODE_4M.ms.fd"
      OPTIONS="${OPTIONS} -drive if=pflash,format=raw,file=OVMF_VARS_4M.ms.fd"
      VGADEV="qxl"
      ;;
    S)
      # set audio device
      OPTIONS="${OPTIONS} -soundhw ${OPTARG:-"all"}"
      ;;
    V)
      # set VGA device
      VGADEV="${OPTARG}"
      ;;
    M)
      # set RAM size
      RAMSIZE="${OPTARG}"
      ;;
    r)
      # run existing image
      RUNONLY=1
      ;;
    i)
      # set installer image to netinst
      TARGET="images/debian-amd64-netinst.iso"
      ;;
    n)
      # disable network
      NETWORK="-nic none"
      ;;
    h)
      # help output
      ;;
  esac
done

shift $((OPTIND-1))

test -e includes.${1%.*} && . includes.${1%.*}

. includes


# setup if -r is not used
if [ $RUNONLY -ne 1 ]
then
  TARGET=${TARGET:-"images/${CDD_BRAND_CD}.iso"}

  qemu-img create -f qcow2 /tmp/qemu-test-${CDD_TARGET}.hd.img "${CDD_SIZE_CD}G"
  cp -v /usr/share/OVMF/OVMF_VARS_4M.ms.fd .

  OPTIONS="${OPTIONS} -cdrom ${TARGET} -boot once=d"
fi

VGADEV=${VGADEV:-"std"}

case ${VGADEV} in
  none)
    OPTIONS="${OPTIONS} -nographic -vga $VGADEV"
    ;;
  *)
    OPTIONS="${OPTIONS} -vga $VGADEV"
    ;;
esac

OPTIONS="${OPTIONS} ${NETWORK:-"-nic user,model=${NETDEV},hostfwd=tcp::10022-:22,hostfwd=tcp::10943-:943,hostfwd=tcp::10443-:443 -nic user,model=${NETDEV}"}"

set -x

qemu-system-x86_64 \
  -enable-kvm \
  -cpu host \
  -smp 2 \
  -m "${RAMSIZE:-4}G" \
  -usb -device usb-tablet -device usb-kbd \
  -hda /tmp/qemu-test-${CDD_TARGET}.hd.img \
  ${OPTIONS} &

exit 0

# vim: set ts=2 sw=2 ai si et:
