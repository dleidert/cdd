#!/bin/sh

set -ex

TDIR="$1"

if [ -n "${DISKTITLE}" ]
then
	sed -i -e "/^menu title/ s#Debian.*\$#${DISKTITLE} Installer menu#" ${TDIR}/${CDD_DIST}/boot*/isolinux/menu.cfg
 	sed -i -e "s#Debian GNU/Linux UEFI Installer menu#${DISKTITLE} UEFI Installer menu#" ${TDIR}/${CDD_DIST}/CD1/boot/grub/theme/*
fi

if [ -n "${GRUBTITLE}" ]
then
	sed -i -e "/^title-text:/ c title-text: \"${GRUBTITLE}\"" ${TDIR}/${CDD_DIST}/CD1/boot/grub/theme/*
fi

exit 0
