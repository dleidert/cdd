#!/bin/sh

set -ex

TDIR="$1"

sed -i -e '/^menu title/ s/Debian/WGDD.ORG Remote Debian/ ; s/ menu.*$//' ${TDIR}/buster/boot*/isolinux/menu.cfg

exit 0
