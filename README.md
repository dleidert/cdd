# Requirements

This is a private project. The following conditions are met by the installer:

* use the text based Debian installer not the graphical front-end
* minimal install for installation on physical hardware and VMs
* ISO to be written to an USB memory stick and installed from there
* ISO to be written to target system and boot network-console
* automatic installation
* automatic reboot when done
* custom title
* load the firmware needed for the installation and have the firmware on the
  USB memory without any network
* firmware packages available for NICs and wifi

# Building the images

## Prerequisites

Install the following packages:

```
sudo apt-get install --install-recommends build-essential debhelper devscripts git simple-cdd
```

and get the sources from Git:

```
git clone git@salsa.debian.org:dleidert/cdd.git
cd cdd
```

## Building the sources

To build image(s):

```
./build.sh [target]
```

If `target` is not given, the default image created via `default.conf` will be
built. Otherwise `target.conf` will be used.

## Testing the image(s) with QEMU

To test a created image use:

```
./qemu.sh [target]
```

Qemu will create some port forwardings to be able to use SSH or OpenVPN AS
admin/client interface and VPN.

# Available targets

## default

Standard installation. Will create a fully automated installer image, which
will start and reboot automatically as well.

## ssh or remote

Creates an installer image which fires up the network-console and starts an
OpenSSH server to continue the installation remotely.

## openvpn (**TODO**)

Like [default](#default), but will also install [OpenVPN AS] and set it up.

[OpenVPN AS]: https://openvpn.net/vpn-software-packages/debian/

## omv (**TODO**)

Like [default](#default), but will also install [Openmediavault].

[Openmediavault]: https://www.openmediavault.org/

# Available profiles

## autoinstall

This is a build profile only and doesn't need to be listed as auto-profile.  It
just sets the `BOOT_TIMEOUT` variable.

## base

Contains the general rules and packages we want to have for every image.

## firmware

Exports the `FORCE_FIRMWARE` environment variable to force debian-cdd to
include the free and non-free firmware packages necessary for networking cards.
It also preseeds the acceptance for several license agreements and enables the
contrin and non-free components.

## network-console

Presseds the installer to load the network-console and an SSH server to
continue the installation via SSH remotely.

## nonet

Preseeds all debconf questions to avoid attempting the internet during
installation. This includes disabling NTP and apt network mirrors.

## openvpn-as

Install the OpenVPN AS package.

## reboot

Small profile to just preseed to reboot automatically after the installation
finishes.

## recommends

Enables to install recommended packages by default.

## standard

This is the standard build profile to set up `KERNEL_PARAMS` and other
variables for the image build. This profile is supposed to always be included.

## vm

A minor profile to detect the installation of the target system in a VM and
install the required guest tools depending on the VM environment.

# Links

* <https://preseed.debian.net/debian-preseed/buster/amd64-main-full.txt>
* <https://www.debian.org/releases/buster/i386/apbs01.en.html>
* <https://www.debian.org/releases/buster/i386/ch05s03.en.html>

<!-- vim: set tw=79 ts=2 sw=2 ai si et: -->
