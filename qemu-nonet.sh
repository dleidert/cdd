#!/bin/bash

set -e

. includes

qemu-img create -f qcow2 images/qemu-test.hd.img 4G
qemu-system-x86_64 \
	-enable-kvm \
	-cpu host \
	-smp 2 \
	-m 4G \
	-k se \
	-nic none \
	-hda images/qemu-test.hd.img \
	-cdrom images/${CDD_BRAND}-${CDD_TARGET}-${CDD_ARCH}.iso \
	-boot once=d

exit 0
