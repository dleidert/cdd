#!/bin/bash
# vim: set ts=2 sw=2 ai si et:

set -e

this_path="$(pwd)"
this_script="$(basename $BASH_SOURCE)"

function apt_conf {
  export APT_CONFIG="${this_path}/tmp/cd-build/apt/${CDD_DIST}-${CDD_ARCH}/apt/apt.conf"

  mkdir -p tmp/cd-build/apt/${CDD_DIST}-${CDD_ARCH}/apt/
  cat << EOF > tmp/cd-build/apt/${CDD_DIST}-${CDD_ARCH}/apt/apt.conf
APT::Default-Release "${CDD_DIST}";
EOF
}

function build_cd {
  retval=0
  echo -e "\e[31mBuilding ${CDD_TARGET} image'\e[0m"
  apt_conf
  build-simple-cdd --dist ${CDD_DIST} --conf ${SIMPLE_CDD_DIR}/${CDD_TARGET}.conf --force-preseed --verbose || retval=$?
  if [ ${retval} -eq 0 ]
  then
    mv -v images/${CDD_ARCHES_CD}.iso \
          images/${CDD_BRAND_CD}.iso
    mv -v images/${CDD_ARCHES_CD}.list.gz \
          images/${CDD_BRAND_CD}.list.gz
  else
    echo "Some error occured."
    exit ${retval}
  fi
  unset APT_CONFIG
}

test -e ${this_path}/includes.${1%.*} && . ${this_path}/includes.${1%.*}

. ${this_path}/includes

echo -e "\e[31m$(date -Ins)\e[0m"

build_cd

exit 0
